package sk.murin.autobazar.model

import jakarta.persistence.*

@Entity(name = "car")
data class CarEntity(
    @GeneratedValue @Id val id: Long = 0L,
    @Column(columnDefinition = "LONGTEXT")
    @OneToMany(fetch = FetchType.LAZY)
    val images: List<ImageEntity> = emptyList(),
    val brand: String = "",
    val age: Int = 5,
    val description: String = "",
    val price: Int = 0
)


fun CarEntity.toDto() = CarDto(
    brand = brand,
    age = age,
    description = description,
    price = price,
    id = id,
    imageIds = images.map { it.id }
)