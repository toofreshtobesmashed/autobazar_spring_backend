package sk.murin.autobazar.model

data class CarEditDto(
    val brand: String = "",
    val age: Int = 5,
    val description: String = "",
    val price: Int = 0
)