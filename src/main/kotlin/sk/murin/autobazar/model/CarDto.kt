package sk.murin.autobazar.model

data class CarDto(
    val id: Long,
    val imageIds: List<Long>,
    val brand: String,
    val age: Int,
    val description: String,
    val price: Int,
)