package sk.murin.autobazar.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id

@Entity(name = "image")
data class ImageEntity(
    @GeneratedValue @Id val id: Long = 0L,
    @Column(columnDefinition = "LONGTEXT")
    val data: String = ""
)