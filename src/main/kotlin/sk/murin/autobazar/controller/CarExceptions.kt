package sk.murin.autobazar.controller

import org.springframework.web.bind.annotation.ControllerAdvice

@ControllerAdvice
class MissingPhotoException : RuntimeException("Missing photo")


@ControllerAdvice
class CarNotFoundException : RuntimeException("Missing car")