package sk.murin.autobazar.controller

 import sk.murin.autobazar.model.CarEntity
import sk.murin.autobazar.model.CarDto
 import sk.murin.autobazar.model.toDto


data class CarListResponse(
    val cars: List<CarDto>
)

fun List<CarEntity>.toDto() = CarListResponse(
    cars = map {
        it.toDto()
    }
)

data class CarSaveResponseDto(
    val id: Long
)


