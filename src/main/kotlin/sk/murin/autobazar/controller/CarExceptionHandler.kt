package sk.murin.autobazar.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class CarExceptionHandler {
    @ExceptionHandler(MissingPhotoException::class)
    fun handleMissingPhotoException(ex: MissingPhotoException) = ResponseEntity.status(HttpStatus.NOT_FOUND.value())
}