package sk.murin.autobazar.controller


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import sk.murin.autobazar.model.CarEditDto
import sk.murin.autobazar.model.toDto
import sk.murin.autobazar.service.CarService

@RestController
@RequestMapping("/cars")
class CarController(val carService: CarService) {
    private val log: Logger = LoggerFactory.getLogger(CarService::class.java)

    @GetMapping
    fun getAllCars() = carService.loadAllCars().toDto().also {
        log.info("simulated get all cars request with response :{}", it.cars.map { it.id })
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) = carService.deleteById(id).also {
        log.info("simulated delete request deleteById with $id and response :{}", it)
    }

    @PutMapping("/{id}")
    fun updateCar(@PathVariable id: Long, @RequestBody car: CarEditDto) = carService.updateCar(id, car).also {
        println("simulated put request with $id and car $car")
    }

    @GetMapping("/{id}")
    fun getOneCar(@PathVariable id: Long) = carService.getCar(id).toDto().also {
        log.info(
            "simulated get request with response" +
                    "id: ${it.id} brand :${it.brand} " +
                    "age: ${it.age} ${it.description} ${it.price} :{}", ""
        )
    }


    @PostMapping
    fun saveCar(@RequestBody car: CarEditDto) = CarSaveResponseDto(carService.saveCar(car)).also {
        log.info("simulated post request with response :{}", it.id)
    }


    @PostMapping("/{id}/photo")
    fun savePhoto(@PathVariable id: Long, @RequestParam("file") file: MultipartFile) = carService.saveImage(id, file)


    @GetMapping("/{id}/photo", produces = [MediaType.IMAGE_PNG_VALUE])
    fun loadPhotoFromDb(@PathVariable id: Long) = carService.getImage(id)


    @GetMapping("/{id}/photo/{imgId}", produces = [MediaType.IMAGE_PNG_VALUE])
    fun loadPhotoFromDbByCarId(
        @PathVariable id: Long,
        @PathVariable imgId: Long,
    ) = carService.getImageByCarId(id, imgId)

}