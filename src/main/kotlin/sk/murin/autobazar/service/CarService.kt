package sk.murin.autobazar.service

import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import sk.murin.autobazar.controller.CarNotFoundException
import sk.murin.autobazar.controller.MissingPhotoException
import sk.murin.autobazar.model.CarEntity
import sk.murin.autobazar.model.CarEditDto
import sk.murin.autobazar.model.ImageEntity
import sk.murin.autobazar.repository.CarImageRepository
import sk.murin.autobazar.repository.CarRepository
import java.util.Base64

@Service
class CarService(
    val carRepository: CarRepository,
    val carImageRepository: CarImageRepository
) {

    fun loadAllCars() = carRepository.findAll()

    fun deleteById(id: Long) = carRepository.deleteById(id)

    fun saveCar(carDto: CarEditDto): Long {
        println(carDto)
        return carRepository.save(
            CarEntity(
                brand = carDto.brand,
                age = carDto.age,
                description = carDto.description,
                price = carDto.price
            )
        ).id

    }

    fun saveImage(carId: Long, file: MultipartFile) {
        val data = String(Base64.getMimeEncoder().encode(file.bytes))
        val car = getCar(carId)
        val img = carImageRepository.save(
            ImageEntity(
                data = data
            )
        )
        val newCar = car.copy(images = car.images + img)
        carRepository.save(newCar)
    }

    fun getCar(carId: Long) = carRepository.findById(carId).orElseThrow { CarNotFoundException() }


    fun getImage(id: Long): ByteArray {
        val car = getCar(id)
        val data = car.images.firstOrNull()?.data ?: throw MissingPhotoException()
        return Base64.getMimeDecoder().decode(data)
    }

    fun getImageByCarId(id: Long, imgId: Long): ByteArray {
        val car = getCar(id)
        val img = car.images.find { it.id == imgId }?.data ?: throw MissingPhotoException()
        return Base64.getMimeDecoder().decode(img)
    }

    fun updateCar(id: Long, car: CarEditDto) = carRepository.save(
        CarEntity(
            id = id,
            brand = car.brand,
            age = car.age,
            description = car.description,
            price = car.price,
            images = getCar(id).images
        )
    )

}