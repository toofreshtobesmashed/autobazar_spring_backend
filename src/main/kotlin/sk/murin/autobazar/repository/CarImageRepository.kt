package sk.murin.autobazar.repository

import org.springframework.data.jpa.repository.JpaRepository
import sk.murin.autobazar.model.ImageEntity


interface CarImageRepository : JpaRepository<ImageEntity, Long>

