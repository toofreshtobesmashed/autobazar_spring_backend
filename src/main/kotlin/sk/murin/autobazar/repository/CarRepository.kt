package sk.murin.autobazar.repository

import org.springframework.data.jpa.repository.JpaRepository
import sk.murin.autobazar.model.CarEntity

interface CarRepository : JpaRepository<CarEntity, Long>



