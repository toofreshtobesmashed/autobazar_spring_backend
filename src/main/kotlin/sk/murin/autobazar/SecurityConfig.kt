package sk.murin.autobazar

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.SecurityFilterChain
import org.springframework.web.cors.CorsConfiguration


@Configuration
@EnableWebSecurity
class SecurityConfig() {
    companion object {
        private val CORS_CONFIG = CorsConfiguration().apply {
            allowedHeaders = listOf("Authorization", "Cache-Control", "Content-Type")
            allowedOriginPatterns = listOf("*")
            allowedMethods = listOf("GET", "POST", "DELETE","PUT")
            allowCredentials = true
            exposedHeaders = listOf("Authorization")
        }
    }

    @Bean
    fun httpConfig(http: HttpSecurity): SecurityFilterChain =
        http.cors().configurationSource { CORS_CONFIG }.and()
            .csrf().disable().authorizeHttpRequests().anyRequest().permitAll().and().build()



   // https://stackoverflow.com/questions/74447778/spring-security-in-spring-boot-3

}


